# Deepwords

## Getting started

Before trying to run the app make sure to install the following tools:

1. Install [Node](https://nodejs.org/en/) (if you're on Mac you can use [Homebrew](https://brew.sh))
2. Install NPM & yarn

After yarn's installed on your local machine, open navigate to the app's directory.
`cd vipon/app`

Install the local dependencies
`yarn install`

To launch the app, start the development server
`yarn start`


## Building the app for production
To bundle the React app for production run:
`yarn build`