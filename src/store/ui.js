import { extendObservable, action } from 'mobx'

class UiStore {
  constructor () {
    extendObservable(this, {
      regionValue: 'all',
      setRegionValue: action(v => {
        this.regionValue = v
      }),
      regionOptions: [
        { value: 'all', text: 'All' },
        { value: 'com', text: 'USA' },
        { value: 'co.uk', text: 'UK' },
        { value: 'ca', text: 'Canada' },
        { value: 'de', text: 'Germany' },
        { value: 'fr', text: 'France' },
        { value: 'co.jp', text: 'Japan' },
        { value: 'it', text: 'Italy' },
        { value: 'cn', text: 'China' },
        { value: 'es', text: 'Spain' },
        { value: 'in', text: 'India' },
        { value: 'com.br', text: 'Brazil' },
      ],
    })
  }
}

const uiStore = new UiStore()
export default uiStore
