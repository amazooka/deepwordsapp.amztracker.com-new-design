import { extendObservable, action } from 'mobx'
import { fetchRegions } from '../lib/api'
import uuidV4 from 'uuid/v4'

class Region {
  constructor ({ id, name }) {
    this.id = id || uuidV4()

    extendObservable(this, {
      name,
    })
  }
}

class RegionStore {
  constructor () {
    extendObservable(this, {
      regions: [],

      loading: false,

      loadRegions: action(() => {
        this.loading = true
        fetchRegions().then(regionsJson => {
          regionsJson.forEach(json => this.addRegionFromServer(json))
          this.loading = false
        })
      }),

      addRegionFromServer: action(json => {
        let region = this.regions.find(region => region.name === json.region)
        if (!region) {
          region = new Region({ name: json.region })
          this.regions.push(region)
        }
      }),
    })

    this.loadRegions()
  }
}

const regionStore = new RegionStore()
export default regionStore
