import { extendObservable, action, computed } from 'mobx'
import { login } from '../lib/api'

class Auth {
  constructor () {
    extendObservable(this, {
      token: localStorage.getItem('deepwords-token') || '',

      isLoggedIn: computed(() => !!this.token),

      login: action((email, password) => {
        login(email, password).then(json => {
          this.token = json.token
          localStorage.setItem('deepwords-token', json.token)
        })
      }),

      logout: action(() => {
        this.token = ''
        localStorage.removeItem('deepwords-token')
      }),
    })
  }
}

const auth = new Auth()
export default auth
