import React from 'react'
import styled from 'styled-components'
import Text from './common/Text'

const LogoIcon = () => (
  <svg viewBox='0 0 48 48' width='24' height='24'>
    <path d='M 4 4 L 44 4 L 44 44 Z' fill='#a88add' />
    <path d='M 4 4 L 34 4 L 24 24 Z' fill='rgba(0,0,0,0.15)' />
    <path d='M 4 4 L 24 4 L 4  44 Z' fill='#0cc2aa' />
  </svg>
)

const Wrap = styled.div`
  padding: 24px;
  background: #fff;
  box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.13);
`
const LeftWrap = styled.div`
  display: flex;
  align-items: center;
  svg {
    margin: 0 4px 0 0;
  }
`

const Header = () => (
  <Wrap>
    <LeftWrap>
      <LogoIcon />
      <Text logo noWrap>DeepWords</Text>
    </LeftWrap>
  </Wrap>
)

export default Header
