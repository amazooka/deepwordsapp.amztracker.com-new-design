import React from 'react'
import styled from 'styled-components'
import Search from './Search'
import KeywordsTable from './KeywordsTable'
import Text from './common/Text'

const Container = styled.div`
  max-width: ${960 + 48}px;
  margin: 0 auto;
  padding: 0 24px;
`
const SearchWrap = styled.div`
  padding: 64px 0 0;
`
const TableWrap = styled.div`
  padding: 32px 0 64px;
`

const PageStepper = styled.div`
  display: inline-flex;
`
const PageStep = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 4px;
  cursor: pointer;
  &:hover { background: #f0f0f0; }
  color: #c5c5c5;
  ${({ active }) => active && `
    background: #f0f0f0;
  `}
`

const Home = () => (
  <Container>
    <SearchWrap>
      <Search />
    </SearchWrap>
    <TableWrap>
      <KeywordsTable />
    </TableWrap>
    <PageStepper>
      {[1, 2, 3, 4, 5, 6, 7].map(number => (
        <PageStep key={number}>
          <Text small noWrap>{number}</Text>
        </PageStep>
      ))}
    </PageStepper>
  </Container>
)

export default Home
