import styled from 'styled-components'

const Input = styled.input`
  width: 100%;
  padding: 12px 16px;
  background: #fff;
  border-radius: 4px;
  border: 1px solid #e7e7e7;
  font-size: 14px;
  line-height: 1;
  &:focus { outline: none; }
  &::-webkit-input-placeholder { color: rgba(41, 41, 41, 0.2); }
  &::-moz-placeholder { color: rgba(41, 41, 41, 0.2); }
  &:-ms-input-placeholder { color: rgba(41, 41, 41, 0.2); }
  &:-moz-placeholder { color: rgba(41, 41, 41, 0.2); }
`

export default Input
