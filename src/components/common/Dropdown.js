import React from 'react'
import styled from 'styled-components'

const Select = styled.select`
  display: block;
  width: 100%;
  height: 40px !important;
  padding: 12px 16px;
  background: ${({ disabled }) => disabled ? '#fafafa' : '#fff'};
  border-radius: 4px;
  border: 1px solid #e7e7e7;
  font-size: 14px;
  line-height: 1;
  cursor: ${({ disabled }) => disabled ? 'not-allowed' : 'auto'};
  user-select: none;
  &:focus { outline: none; }
`

const Dropdown = ({ disabled, value, options, onChange }) => (
  <Select disabled={disabled} value={value} onChange={e => onChange(e.target.value)}>
    {options.map(({ value, text }) => (
      <option key={value} value={value}>{text}</option>
    ))}
  </Select>
)

export default Dropdown
