import styled from 'styled-components'

const Text = styled.div`
  word-wrap: break-word;
  font-weight: 400;

  ${({noWrap}) => noWrap && `
    line-height: 1;
    white-space: nowrap;
  `}

  ${({ logo }) => logo && `
    font-size: 17px;
    font-weight: 600;
    letter-spacing: -0.1px;
  `}
  ${({ small }) => small && `
    font-size: 14px;
    font-weight: 600;
  `}
`

export default Text
