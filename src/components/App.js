import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { injectGlobal } from 'styled-components'
import Helmet from 'react-helmet'
import Header from './Header'
import Home from './Home'
import Login from './Login'

injectGlobal([`
  *, *:before, *:after {
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
  }

  body {
    width: 100%;
    margin: 0;
    background-color: #fafafa;
    font-family: 'Proxima Nova', ProximaNova, -apple-system, BlinkMacSystemFont, sans-serif;
    line-height: 1.4;
    color: #292929;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: 'grayscale';
  }

  a, button, input {
    font-family: 'Proxima Nova', ProximaNova, -apple-system, BlinkMacSystemFont, sans-serif;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: 'grayscale';
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  svg {
    display: block;
  }
`])

const App = () => (
  <Router>
    <div>
      <Helmet
        title='DeepWords'
        meta={[{ name: 'description', content: `A react app` }]}
        link={[{ rel: 'shortcut icon', href: require('../static/favicon.png') }]}
      />
      <Header />
      <Route exact path='/' component={Home} />
      <Route path='/login' component={Login} />
    </div>
  </Router>
)

export default App
