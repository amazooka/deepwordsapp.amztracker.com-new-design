import React from 'react'
import styled from 'styled-components'

const Wrap = styled.div`
  padding: 80px;
  text-align: center;
`
const Form = styled.div`
  display: inline-block;
  width: 100%;
  max-width: 320px;
`
const Input = styled.input`
  margin: 0 0 16px;
  width: 100%;
  padding: 8px;
  font-size: 16px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  &:focus { outline: none; }
`
const Button = styled.button`
  padding: 8px;
  border: 0;
  border-radius: 2px;
  font-size: 14px;
  background: rgba(0, 0, 0, 0.1);
  cursor: pointer;
  &:focus { outline: none; }
`

const Login = () => (
  <Wrap>
    <Form>
      <Input type='email' placeholder='Email' required />
      <Input type='password' placeholder='Password' required />
      <Button>Login</Button>
    </Form>
  </Wrap>
)

export default Login
