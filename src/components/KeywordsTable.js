import React from 'react'
import { injectGlobal } from 'styled-components'
import { Table, Column, AutoSizer } from 'react-virtualized'
import 'react-virtualized/styles.css'

injectGlobal([`
  .ReactVirtualized__Table {
    font-size: 14px;
  }
  .ReactVirtualized__Table *:focus {
    outline: none;
  }
  .ReactVirtualized__Table__headerColumn {
    font-size: 12px;
    font-weight: 600;
    color: #c5c5c5;
  }
  .ReactVirtualized__Table__headerColumn, .ReactVirtualized__Table__rowColumn {
    margin: 0 16px 0 0 !important;
  }
  .ReactVirtualized__Table__headerColumn:first-of-type, .ReactVirtualized__Table__rowColumn:first-of-type {
    margin: 0 16px !important;
  }
  .ReactVirtualized__Table__rowColumn.keywords {
    font-weight: 600;
  }
  .ReactVirtualized__Table__row.odd {
    background: #fff;
  }
`])

const results = [
  { keyword: 'cutting board', emsv: 915000, categories: 'All Departments', region: 'USA' },
  { keyword: 'stock market board game', emsv: 805000, categories: 'All Departments', region: 'USA' },
  { keyword: 'balance board', emsv: 710000, categories: 'All Departments', region: 'USA' },
  { keyword: 'fantasy board games', emsv: 655000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
  { keyword: 'running boards', emsv: 451000, categories: 'All Departments', region: 'USA' },
]

const getRowClassName = ({ index }) => {
  if (index < 0) return ''
  return index % 2 === 0 ? 'odd' : 'even'
}

const KeywordsTable = () => (
  <AutoSizer disableHeight>
    {({ width }) => (
      <Table
        width={width}
        height={640}
        headerHeight={24}
        rowHeight={48}
        rowCount={results.length}
        rowGetter={({ index }) => results[index]}
        rowClassName={getRowClassName}
      >
        <Column
          className='keywords'
          label='Keyword'
          dataKey='keyword'
          width={280}
        />
        <Column
          label='Estimated Monthly Search Volume'
          dataKey='emsv'
          width={280}
        />
        <Column
          label='Categories'
          dataKey='categories'
          width={320}
        />
        <Column
          label='Region'
          dataKey='region'
          width={80}
        />
      </Table>
    )}
  </AutoSizer>
)

export default KeywordsTable
