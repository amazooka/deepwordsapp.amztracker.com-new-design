import React from 'react'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'
import { inject, observer } from 'mobx-react'
import Text from './common/Text'
import Input from './common/Input'
import Dropdown from './common/Dropdown'

const Wrap = styled.div`
  display: flex;
  align-items: flex-end;
`
const SearchItem = styled.div`
  width: 100%;
  margin: 0 8px 0 0;
`
const Label = styled.div`
  padding: 0 0 4px;
`
const Button = styled.button`
  padding: 12px 20px;
  background: #05cefe;
  border: 1px solid transparent;
  border-radius: 4px;
  font-size: 14px;
  font-weight: 600;
  line-height: 1;
  color: #fff;
  cursor: pointer;
  &:active { background: #01c7f9; }
  &:focus { outline: none; }
`

const Search = ({ regionValue, regionOptions, onRegionChange }) => (
  <Wrap>
    <SearchItem>
      <Label>
        <Text small noWrap>Keyword</Text>
      </Label>
      <Input type='text' placeholder='Enter keyword or phrase...' />
    </SearchItem>
    <SearchItem>
      <Label>
        <Text small noWrap>Region</Text>
      </Label>
      <Dropdown value={regionValue} options={regionOptions} onChange={onRegionChange} />
    </SearchItem>
    <SearchItem>
      <Label>
        <Text small noWrap>Categories</Text>
      </Label>
      <Dropdown disabled value={'all'} options={[{ value: 'all', text: 'All' }]} />
    </SearchItem>
    <Button>Search</Button>
  </Wrap>
)

export default compose(
  inject('uiStore'),
  observer,
  withProps(({ uiStore }) => ({
    regionValue: uiStore.regionValue,
    regionOptions: uiStore.regionOptions,
    onRegionChange: uiStore.setRegionValue,
  })),
)(Search)
