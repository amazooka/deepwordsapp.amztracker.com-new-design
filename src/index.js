import 'airbnb-js-shims'
import 'normalize.css'
import initReactFastclick from 'react-fastclick'
import React from 'react'
import ReactDOM from 'react-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import uiStore from './store/ui'
import regionStore from './store/region'
import auth from './store/auth'
import App from './components/App'

initReactFastclick()
useStrict()

auth.login('jacqjaca@gmail.com', 'wehavethedata!')

ReactDOM.render(
  <Provider uiStore={uiStore} regionStore={regionStore} auth={auth}>
    <App />
  </Provider>,
  document.getElementById('root'),
)
