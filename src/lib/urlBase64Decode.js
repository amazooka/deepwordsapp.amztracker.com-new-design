const urlBase64Decode = (str) => {
  let output = str.replace('-', '+').replace('_', '/')
  switch (output.length % 4) {
    case 0:
      break
    case 2:
      output += '=='
      break
    case 3:
      output += '='
      break
    default:
      throw new Error('Illegal base64url string!')
  }
  return window.atob(output)
}

export default urlBase64Decode
