const baseUrl = 'https://deepwordsapp.amztracker.com/api/deepwords'
const authUrl = 'https://deepwordsapp.amztracker.com/api/auth'

const config = {
  headers: {
    Authorization: `Bearer ${localStorage.getItem('deepwords-token')}`,
  },
}

export const login = async (email, password) => {
  const res = await fetch(`${authUrl}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      source: 'deepwords',
      email,
      password,
      language: 'en',
    }),
  })

  const data = await res.json()
  return data
}

export const fetchRegions = async () => {
  const res = await fetch(`${baseUrl}/regions`, config)
  const data = await res.json()
  return data
}
